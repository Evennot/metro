//
//  MLine.m
//  Metro
//
//  Created by Danila Parhomenko on 4/1/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MLine.h"
#import "MCar.h"
#import "MPerson.h"
#import "MWayPoint.h"

#define PossibleInstationOffset -10
// ^^ make it positive to activate

#define MinZigzagLength 20
#define EndSize 40.0
#define LineW (20*sqrt(2)/8)


@interface MLine ()

@property (nonatomic) BOOL cyclic;
@property (nonatomic, weak) MStop *hoveringOnStop;
@property (nonatomic, strong) NSMutableArray *cars;

@end

@implementation MLine {
    struct {
        CGPoint p;
    } ps[200];
    NSUInteger lastCount;
    NSInteger lastStopsConnected;
    BOOL changed;
    
    //enumerating context:
    CGFloat enumerationDx;
    CGFloat enumerationDy;
#warning add freeze to unchanged ps
}

- (id) initWithColor:(UIColor *)color startStation:(MStation *)station Delegate:(id<MapProtocol>)delegate {
    if (self = [super init]) {
        if (![station tryConnectToLine:self inDirX:0 inDirY:0 outDirX:0 outDirY:0])
            return nil;
        self.stations = [NSMutableArray arrayWithObject:station];
        self.stops = [NSMutableArray arrayWithObject:station];
        self.lineWidth = 20.0;
        [self setStrokeColor:color];
        self.delegate = delegate;
        lastCount = 0;
        lastStopsConnected = 0;
        //[self setFillColor:color];
    }
    return self;
}

int directionValue(CGFloat d) {
    if (d > 0.01)
        return 1;
    if (d > -0.01)
        return 0;
    return -1;
}

CGFloat squareDistance(CGPoint p1, CGPoint p2) {
    return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
}

CGFloat angleByDirections(int dirX, int dirY) {
    switch (dirX) {
        case -1:
            switch (dirY) {
                case -1: return -3*M_PI_4;
                case 1:  return 3*M_PI_4;
                default: return M_PI;
            }
        case 1:
            switch (dirY) {
                case -1: return -M_PI_4;
                case 1:  return M_PI_4;
                default: return 0;
            }
        default:
            if (dirY == -1)
                return -M_PI/2.0;
            return M_PI/2.0;
    }
}

- (BOOL) calcDxDyBetweenStart:(CGPoint)s1 end:(CGPoint)s2 intermediateDSBlock:(BOOL(^)(CGPoint)) intermediateCallback {
    CGFloat newDx = s2.x - s1.x;
    CGFloat newDy = s2.y - s1.y;
    CGFloat absDx = ABS(newDx);
    CGFloat absDy = ABS(newDy);
    if ((absDx < 0.01) && (absDy < 0.01)) return NO;
    CGFloat minD = MIN(ABS(newDx), ABS(newDy));
    int previosXDirection = directionValue(enumerationDx);
    int previosYDirection = directionValue(enumerationDy);
    int newXDirection = directionValue(newDx);
    int newYDirection = directionValue(newDy);

    while (true) {
        int yDiffer;
        int xDiffer;
        if ((previosXDirection == 0) && (previosYDirection == 0)) {
            xDiffer = 0;
            yDiffer = 0;
        } else {
            xDiffer = ABS(previosXDirection - newXDirection);
            yDiffer = ABS(previosYDirection - newYDirection);
        }
        switch (xDiffer) {
            case 1:
                switch (yDiffer) {
                    case 1: // Pi/2 difference
                        if (previosXDirection == 0) {
                            enumerationDx = MinZigzagLength * newXDirection;
                            enumerationDy = MinZigzagLength * previosYDirection;
                        } else {
                            enumerationDx = MinZigzagLength * previosXDirection;
                            enumerationDy = MinZigzagLength * newYDirection;
                        }
                        break;
                    case 2:  // 3*Pi/4 difference
                        enumerationDx = MinZigzagLength * previosXDirection;
                        enumerationDx = 0;
                    default:  // Pi/4 difference
                        if (minD < 0.01) {
                            enumerationDx = newDx;
                            enumerationDy = newDy;
                        } else {
                            enumerationDx = minD*newXDirection;
                            enumerationDy = minD*newYDirection;
                        }
                        break;
                }
                break;
            case 2:
                switch (yDiffer) {
                    case 1: // 3*Pi/4 difference
                        enumerationDx = MinZigzagLength * previosXDirection;
                        enumerationDy = MinZigzagLength * newYDirection;
                        break;
                    case 2: // 2*Pi difference
                        enumerationDx = MinZigzagLength * previosXDirection;
                        enumerationDy = 0;
                        break;
                    default:
                        enumerationDx = 0;
                        enumerationDy = MinZigzagLength * previosYDirection;
                        break;
                }
                break;
            default:
                switch (yDiffer) {
                    case 1: // Pi/4 difference
                        if (minD < 0.01) {
                            enumerationDx = newDx;
                            enumerationDy = newDy;
                        } else {
                            enumerationDx = minD*newXDirection;
                            enumerationDy = minD*newYDirection;
                        }
                        break;
                    case 2: // Pi/2 difference
                        enumerationDx = MinZigzagLength*newXDirection;
                        enumerationDy = 0;
                        break;
                    default: // same
                        if (minD < 0.01) {
                            enumerationDx = newDx;
                            enumerationDy = newDy;
                        } else {
                            enumerationDx = minD*newXDirection;
                            enumerationDy = minD*newYDirection;
                        }
                        break;
                }
                break;
        }
        /*CGPoint newS1 = CGPointMake(s1.x + *dx, s1.y + *dy);
        if ([self.delegate point:newS1 belongsToLineOtherThan:self]) {
            int dirX = directionValue(*dx);
            int dirY = directionValue(*dy);
            CGFloat correctedDx = 0;
            CGFloat correctedDy = 0;
            CGFloat o = 0;
            if (dirX) {
                if (dirY) {
                    o = MIN(ABS(*dx), ABS(*dy));
                    if (o > MinZigzagLength) {
                        correctedDx = MinZigzagLength;
                        correctedDy = MinZigzagLength;
                    } else {
                        correctedDx = o;
                        correctedDy = o;
                    }
                } else {
                    o = ABS(*dx);
                    correctedDx = MIN(o, MinZigzagLength);
                }
            } else {
                o = ABS(*dy);
                correctedDy = MIN(o, MinZigzagLength);
            }
            newS1.x = s1.x + correctedDx*dirX;
            newS1.y = s1.y + correctedDy*dirY;
            if (![self.delegate point:newS1 belongsToLineOtherThan:self]) {
                s1 = newS1;
                *dx = correctedDx*dirX;
                *dy = correctedDy*dirY;
            } else // give up
                s1 = CGPointMake(s1.x + *dx, s1.y + *dy);
        } else*/
            s1 = CGPointMake(s1.x + enumerationDx, s1.y + enumerationDy);
        if (!intermediateCallback(s1))
            return NO;
        newDx = s2.x - s1.x;
        newDy = s2.y - s1.y;
        absDx = ABS(newDx);
        absDy = ABS(newDy);
        if ((absDx < 0.01) && (absDy < 0.01)) return YES;
        minD = MIN(ABS(newDx), ABS(newDy));
        previosXDirection = newXDirection;
        previosYDirection = newYDirection;
        newXDirection = directionValue(newDx);
        newYDirection = directionValue(newDy);
    }
}

- (void) moveTo:(CGPoint)p OutOfStation:(BOOL)outOfStation {
    [self genericMoveTo:p OutOfStation:outOfStation Finalize:NO];
}

- (void) genericMoveTo:(CGPoint)p OutOfStation:(BOOL)outOfStation Finalize:(BOOL)finalizeEnds {
    __block int index = 3;
    __block MStop *s1 = nil;
#warning check lines collisions here

    __block BOOL calculationFailed = NO;
    BOOL (^dxdyBlock)(CGPoint) = ^(CGPoint ip){
        if (index < 100) {
            if (!CGPointEqualToPoint(ps[index - 1].p, ip))
                ps[index++].p = ip;
            return YES;
        }
        return NO;
    };
    __block BOOL firstUncertainStation = YES;
    [self.stops enumerateObjectsUsingBlock:^(MStop *s, NSUInteger idx, BOOL *stop) {
        CGPoint stationPosition = [s positionForLine:self andCar:nil];
        if (idx < lastStopsConnected) {
            while ((index < lastCount) && !CGPointEqualToPoint(ps[index++].p, stationPosition)) {
            }
            if (index >= lastCount) {
                calculationFailed = YES;
                *stop = YES;
            }
        } else {
            if (index > 4) {
                enumerationDx = ps[index - 1].p.x - ps[index - 2].p.x;
                enumerationDy = ps[index - 1].p.y - ps[index - 2].p.y;
            } else {
                enumerationDx = 0;
                enumerationDy = 0;
            }
            BOOL passed = NO;
            if (firstUncertainStation) {
                for (int testIndex = index; testIndex < lastCount; ) {
                    if (CGPointEqualToPoint(ps[testIndex++].p, stationPosition)) {
                        index = testIndex;
                        passed = YES;
                        break;
                    }
                }
                firstUncertainStation = NO;
            }
            if (!passed) {
                if (s1 == nil) {
                    ps[index++].p = stationPosition;
                } else {
                    if ([self calcDxDyBetweenStart:[s1 positionForLine:self andCar:nil] end:stationPosition intermediateDSBlock:dxdyBlock]) {
                        dxdyBlock(stationPosition);
                    }
                }
            }
        }
        s1 = s;
    }];
    if (calculationFailed) {
        lastStopsConnected = 0;
        [self genericMoveTo:p OutOfStation:outOfStation Finalize:finalizeEnds];
        return;
    }
    if (finalizeEnds) {
        enumerationDx = ps[index - 1].p.x - ps[index - 2].p.x;
        enumerationDy = ps[index - 1].p.y - ps[index - 2].p.y;
        CGFloat absDX = ABS(enumerationDx);
        CGFloat absDY = ABS(enumerationDy);
        if (absDX > 0.01)
            enumerationDx = enumerationDx * EndSize / absDX;
        else
            enumerationDx = 0;
        if (absDY > 0.01)
            enumerationDy = enumerationDy * EndSize / absDY;
        else
            enumerationDy = 0;
        ps[index].p.x = ps[index - 1].p.x + enumerationDx;
        ps[index].p.y = ps[index - 1].p.y + enumerationDy;
        ps[index + 1].p.x = ps[index].p.x + 0.5*enumerationDy;
        ps[index + 1].p.y = ps[index].p.y - 0.5*enumerationDx;
        ps[index + 2].p.x = ps[index].p.x - 0.5*enumerationDy;
        ps[index + 2].p.y = ps[index].p.y + 0.5*enumerationDx;
        index += 3;
    } else {
        if ([self calcDxDyBetweenStart:[s1 positionForLine:self andCar:nil] end:p intermediateDSBlock:dxdyBlock]) {
            dxdyBlock(p);
        }
    }
    if (index > 4) {
        enumerationDx = ps[4].p.x - ps[3].p.x;
        enumerationDy = ps[4].p.y - ps[3].p.y;
        CGFloat absDX = ABS(enumerationDx);
        CGFloat absDY = ABS(enumerationDy);
        if (absDX > 0.01)
            enumerationDx = enumerationDx * EndSize / absDX;
        else
            enumerationDx = 0;
        if (absDY > 0.01)
            enumerationDy = enumerationDy * EndSize / absDY;
        else
            enumerationDy = 0;
        ps[2].p.x = ps[3].p.x - enumerationDx;
        ps[2].p.y = ps[3].p.y - enumerationDy;
        ps[1].p.x = ps[2].p.x + 0.5*enumerationDy;
        ps[1].p.y = ps[2].p.y - 0.5*enumerationDx;
        ps[0].p.x = ps[2].p.x - 0.5*enumerationDy;
        ps[0].p.y = ps[2].p.y + 0.5*enumerationDx;
    }
    lastCount = index;
    lastStopsConnected = self.stops.count - 1;
    changed = YES;
    if (outOfStation) {
        self.hoveringOnStop = nil;
    }
}

- (void) finalizeEnds {
    [self genericMoveTo:CGPointZero OutOfStation:YES Finalize:YES];
    [self runCar];
}

- (void) draggedToStop:(MStop *)stop {
    if ([self.stops containsObject:stop]) {
        if ([self.hoveringOnStop isEqual:stop])
            return;
        if (self.stops.count > 1) {
            if ([[self.stops lastObject] isEqual:stop]) {
                [self.stops removeObject:stop];
                lastStopsConnected--;
                if ([stop isKindOfClass:[MStation class]])
                    [self.stations removeObject:stop];
                [stop disconnectFromLine:self];
                for (MCar *car in self.cars) {
                    if (car.carState != csDisappearing) {
                        if ([car.upcomingStop isEqual:stop] || [car.previousStop isEqual:stop])
                            car.carState = csRouteInvalidated;
                    }
                }
            }
        }
    } else {
        if (![self.hoveringOnStop isEqual:stop]) {
            if ([stop tryConnectToLine:self inDirX:0 inDirY:0 outDirX:0 outDirY:0]) {
                [self.stops addObject:stop];
                if ([stop isKindOfClass:[MStation class]])
                    [self.stations addObject:stop];
            }
        }
    }
    self.hoveringOnStop = stop;
#warning check for double adding
}

- (BOOL) tryDragging:(CGPoint) p {
    CGFloat r = squareDistance(p, ps[2].p);
    if (r < EndSize*EndSize) {
        // reversing
        for (int i = lastCount / 2; i--; ) {
            CGPoint rp = ps[i].p;
            ps[i].p = ps[lastCount - 1 - i].p;
            ps[lastCount - 1 - i].p = rp;
        }
        for (int i = self.stops.count / 2; i--; ) {
            [self.stops exchangeObjectAtIndex:i withObjectAtIndex:self.stops.count - 1 - i];
        }
        for (int i = self.stations.count / 2; i--; ) {
            [self.stations exchangeObjectAtIndex:i withObjectAtIndex:self.stations.count - 1 - i];
        }
        for (MCar *car in self.cars) {
            if (car.carState == csRunningForward)
                car.carState = csRunningBackward;
            else
                if (car.carState == csRunningBackward)
                    car.carState = csRunningForward;
        }
        lastStopsConnected--;
        [self moveTo:p OutOfStation:YES];
        return YES;
    }
    r = squareDistance(p, ps[lastCount - 3].p);
    if (r < EndSize*EndSize) {
        lastStopsConnected--;
        [self moveTo:p OutOfStation:YES];
        return YES;
    }
    return NO;
}

- (void) returnCarContents:(MCar *)car {
    for (MPerson *p in car.persons) { // returning all persons to source stations
        p.position = [p.source addPerson:p];
        if (p.parent == nil)
            [self.scene addChild:p];
    }
    [car.persons removeAllObjects];
}

- (void) removeFromParent {
    @synchronized (self) {
        for (MCar *car in self.cars) {
            [car removeAllActions];
            [car removeFromParent];
            [self returnCarContents:car];
        }
        for (MStop *s in self.stops) {
            [s disconnectFromLine:self];
        }
    }
    [super removeFromParent];
}

- (void) carDisappeared {
    @synchronized (self) {
        for (MCar *car in self.cars) {
            if (car.alpha < 0.1) {
                [car removeFromParent];
                [self returnCarContents:car];
                [self.cars removeObject:car];
                break;
            }
        }
}
}

- (void) commitAnimations {
    if (changed) {
        for (MCar *car in self.cars) {
            if (car.carState == csRouteInvalidated) {
                if ((car.upcomingStop == nil) || ([self.stops indexOfObject:car.upcomingStop] == NSNotFound)) {
                    car.carState = csDisappearing;
                    [car runAction:[SKAction sequence:@[[SKAction fadeAlphaTo:0 duration:0.5], [SKAction performSelector:@selector(carDisappeared) onTarget:self]]]];
                } else {
                    car.carState = csRunningForward;
                    [self updateCarRoute:car];
                }
            } else {
                //car.carState = csNone;
            }
        }
        CGMutablePathRef pathToDraw = CGPathCreateMutable();
    
        CGPathMoveToPoint(pathToDraw, NULL, ps[0].p.x, ps[0].p.y);
        int dirX = 2;
        int dirY = 2;
        for (int i = 1; i < lastCount; i++) {
            if (i < lastCount - 1) {
                CGFloat x = ps[i].p.x;
                CGFloat y = ps[i].p.y;
                //dirX = directionValue(ps[i].p.x - ps[i - 1].p.x);
                int newDirX = directionValue(ps[i + 1].p.x - ps[i].p.x);
                //dirY = directionValue(ps[i].p.y - ps[i - 1].p.y);
                int newDirY = directionValue(ps[i + 1].p.y - ps[i].p.y);
                if ((dirX == newDirX) || (dirY == newDirY)) {
                    x += dirX * LineW;
                    y += dirY * LineW;
                    CGPathAddLineToPoint(pathToDraw, NULL, x, y);
                    CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y);
                    CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x - newDirX * LineW, ps[i].p.y - newDirY * LineW);
                } else {
                    CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y);
                }
                dirX = newDirX;
                dirY = newDirY;
            } else {
                CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y);
            }
        }
        // lower right border
//            CGFloat dx = ps[i].p.x - ps[i - 1].p.x;
//            CGFloat dy = ps[i].p.y - ps[i - 1].p.y;
//            if (dx > 0.01) {
//                if (dy > 0.01) {
//                    CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*- LineW*/, ps[i].p.y + LineW);
//                } else {
//                    if (dy > -0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y + LineW);
//                    } else {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*+ LineW*/, ps[i].p.y + LineW);
//                    }
//                }
//            } else {
//                if (dx > -0.01) {
//                    if (dy > 0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x - LineW, ps[i].p.y);
//                    } else {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x + LineW, ps[i].p.y);
//                    }
//                } else {
//                    if (dy > 0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*+ LineW*/, ps[i].p.y - LineW);
//                    } else {
//                        if (dy > -0.01) {
//                            CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y - LineW);
//                        } else {
//                            CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*- LineW*/, ps[i].p.y - LineW);
//                        }
//                    }
//                }
//            }
//        }
//        // upper left border
//        for (int i = lastCount - 1; i--;) {
//            //CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y);
//            CGFloat dx = ps[i].p.x - ps[i + 1].p.x;
//            CGFloat dy = ps[i].p.y - ps[i + 1].p.y;
//            if (dx > 0.01) {
//                if (dy > 0.01) {
//                    CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*+ LineW*/, ps[i].p.y - LineW);
//                } else {
//                    if (dy > -0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y - LineW);
//                    } else {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*- LineW*/, ps[i].p.y - LineW);
//                    }
//                }
//            } else {
//                if (dx > -0.01) {
//                    if (dy > 0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*+ LineW*/, ps[i].p.y);
//                    } else {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*- LineW*/, ps[i].p.y);
//                    }
//                } else {
//                    if (dy > 0.01) {
//                        CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*- LineW*/, ps[i].p.y + LineW);
//                    } else {
//                        if (dy > -0.01) {
//                            CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x, ps[i].p.y + LineW);
//                        } else {
//                            CGPathAddLineToPoint(pathToDraw, NULL, ps[i].p.x /*+ LineW*/, ps[i].p.y + LineW);
//                        }
//                    }
//                }
//            }
//        }

        self.path = pathToDraw;
        changed = NO;
    }
}

- (void) selectNextUpcomingStopForCar:(MCar *)car {
    NSInteger idx = [self.stops indexOfObject:car.upcomingStop];
    car.previousStop = car.upcomingStop;
    if (car.carState == csRunningForward) {
        if (idx == self.stops.count - 1) {
            car.carState = csRunningBackward;
            car.upcomingStop = self.stops[idx - 1];
        } else {
            car.upcomingStop = self.stops[idx + 1];
        }
    } else {
        if (idx == 0) {
            car.carState = csRunningForward;
            car.upcomingStop = self.stops[idx + 1];
        } else {
            car.upcomingStop = self.stops[idx - 1];
        }
    }
}

- (BOOL) tryPassWaipointForCar:(MCar *)car {
    __block NSMutableArray *areaAhead = nil;
    [self.stations enumerateObjectsWithOptions:(car.carState == csRunningBackward)?NSEnumerationReverse:0
                                    usingBlock:^(MStop *s, NSUInteger idx, BOOL *stop) {
                                        if (areaAhead) {
                                            if ([s isKindOfClass:[MWayPoint class]])
                                                *stop = YES;
                                            else
                                                [areaAhead addObject:s];
                                        } else {
                                            if ([s isEqual:car.upcomingStop])
                                                areaAhead = [NSMutableArray arrayWithObject:s];
                                        }
                                    }];
    if (areaAhead) {
        for (MCar *c in self.cars)
            if (![c isEqual:car]) {
//                if ([car.upcomingStop isEqual:c.upcomingStop]) {
//                    if ([areaAhead containsObject:c.previousStop])
//                        return NO;
//                } else {
                    if ([areaAhead containsObject:c.upcomingStop] || [areaAhead containsObject:c.previousStop]) {
                        return NO;
                    }
//                }
            }
        SKAction *carRunAction = [self car:car moveToStop:car.upcomingStop Forward:car.carState == csRunningForward];
        [car runAction:carRunAction];
        return YES;
    } else
        return NO;
}

- (void) updateCarRoute:(MCar *)car {
    NSInteger idx = [self.stops indexOfObject:car.upcomingStop];
    if ((idx == NSNotFound) || (self.stations.count < 2)) {
        car.carState = csRouteInvalidated;
        car.upcomingStop = nil;
    } else {
        if ([car.upcomingStop isKindOfClass:[MStation class]]) {
            SKAction *carLoadingAct = [self carLoadingAction:car];
            [self selectNextUpcomingStopForCar:car];
            SKAction *carRunAction = [self car:car moveToStop:car.upcomingStop Forward:car.carState == csRunningForward];
            if (carLoadingAct) {
                carRunAction = [SKAction sequence:@[carLoadingAct, carRunAction]];
            }
            [car runAction:carRunAction];
        } else {
            MWayPoint *wp = (MWayPoint *)car.upcomingStop;
            [self selectNextUpcomingStopForCar:car];
            @synchronized (wp) {
                if (wp.carsWaiting == nil)
                    wp.carsWaiting = [NSMutableArray array];
                [wp.carsWaiting addObject:car];
                NSInteger idx = 0;
                while (idx < wp.carsWaiting.count) {
                    BOOL passed = NO;
                    if ([self tryPassWaipointForCar:wp.carsWaiting[idx]]) {
                        passed = YES;
                        [wp.carsWaiting removeObjectAtIndex:idx];
                    } else
                        idx++;
                }
            }
        }
    }
}

- (BOOL) pointBelongsToLine:(CGPoint) p {
    return [self point:p belongingIndexForEnumeratingForward:YES] != NSNotFound;
}

- (NSInteger) point:(CGPoint)p1 belongingIndexForEnumeratingForward:(BOOL)forward {
    NSInteger nextIndex = NSNotFound;
    for (int i = forward?lastCount - 4:3; (i > 2) && (i < lastCount - 3); forward?i--:i++) {
        CGFloat nx;
        CGFloat ny;
        if (forward) {
            nx = ps[i - 1].p.x;
            ny = ps[i - 1].p.y;
        } else {
            nx = ps[i + 1].p.x;
            ny = ps[i + 1].p.y;
        }
        CGFloat x = ps[i].p.x;
        CGFloat dx = nx - x;
        int dirX = directionValue(dx);
        CGFloat percent;
        if (dirX == -1) {
            if ((p1.x > x) || (p1.x < nx))
                continue;
            percent = (x - p1.x) / (x - nx);
        } else {
            if (dirX == 0) {
                if (ABS(p1.x - x) > 0.01)
                    continue;
                percent = -1;
            } else {
                if ((p1.x < x) || (p1.x > nx))
                    continue;
                percent = (x - p1.x) / (x - nx);
            }
        }
        CGFloat y = ps[i].p.y;
        CGFloat dy = ny - y;
        int dirY = directionValue(dy);
        if (dirY == -1) {
            if ((p1.y > y) || (p1.y < ny))
                continue;
            if (percent < 0) {
                nextIndex = i;
                break;
            } else {
                CGFloat ypercent = (y - p1.y) / (y - ny);
                if (ABS(ypercent - percent) < 0.01) {
                    nextIndex = i;
                    break;
                }
            }
        } else {
            if (dirY == 0) {
                if (ABS(p1.y - y) > 0.01)
                    continue;
                nextIndex = i;
                break;
            } else {
                if ((p1.y < y) || (p1.y > ny))
                    continue;
                if (percent < 0) {
                    nextIndex = i;
                    break;
                } else {
                    CGFloat ypercent = (y - p1.y) / (y - ny);
                    if (ABS(ypercent - percent) < 0.01) {
                        nextIndex = i;
                        break;
                    }
                }
            }
        }
    }
    return nextIndex;
}

- (SKAction *)car:(MCar *) car moveToStop:(MStop *)s Forward:(BOOL)forward {
    CGPoint p1 = car.position;
    int dirX = 0;
    int dirY = 0;
    NSInteger nextIndex = [self point:p1 belongingIndexForEnumeratingForward:forward];
    NSMutableArray *actions = [[NSMutableArray alloc] init];
    if (nextIndex == NSNotFound) {
        [actions addObject:[SKAction moveTo:[s positionForLine:self andCar:nil] duration:0.0]];
#warning calc speed
    } else {
        BOOL gotAngle = YES;
        CGFloat angle = car.angle;
        while (true) {
            CGFloat dx = ps[nextIndex].p.x;
            CGFloat dy = ps[nextIndex].p.y;
            if (forward) {
                dx -= ps[nextIndex - 1].p.x;
                dy -= ps[nextIndex - 1].p.y;
            } else {
                dx -= ps[nextIndex + 1].p.x;
                dy -= ps[nextIndex + 1].p.y;
            }
            CGFloat r = sqrtf(dx*dx + dy*dy);
            dirX = directionValue(dx);
            dirY = directionValue(dy);
            if (r > 10) {
                CGFloat x = angleByDirections(dirX, dirY);
                if (gotAngle) {
                    if (x - angle > M_PI)
                        [actions addObject:[SKAction rotateByAngle:x - angle - 2*M_PI duration:0.1]];
                    else
                        if (x - angle < -M_PI)
                            [actions addObject:[SKAction rotateByAngle:x - angle + 2*M_PI duration:0.1]];
                        else
                            [actions addObject:[SKAction rotateByAngle:x - angle duration:0.1]];
                } else {
                    gotAngle = YES;
                    [actions addObject:[SKAction rotateToAngle:x duration:0.0]];
                }
                angle = x;
            }
            car.angle = angle;
            CGPoint stopP = [s positionForLine:self andCar:nil];
            [actions addObject:[SKAction moveTo:ps[nextIndex].p duration:r/188]];
            if (CGPointEqualToPoint(stopP, ps[nextIndex].p))
                break;
            if (forward) {
                nextIndex++;
                if (nextIndex == lastCount - 3)
                    break;
            } else {
                nextIndex--;
                if (nextIndex <= 2)
                    break;
            }
        }
    }
    [actions addObject:[SKAction performSelector:@selector(updateCarRoute) onTarget:car]];
    return [SKAction sequence:actions];
}

- (SKAction *)carLoadingAction:(MCar *)car {
    MStop *s = car.upcomingStop;
    if (![s isKindOfClass:[MStation class]])
        return nil;
    SKAction *a1 = [((MStation *)s) unloadingActionForCar:car AtLine:self];
    SKAction *a2 = [((MStation *)s) loadingActionForCar:car AtLine:self];
    if (a1) {
        if (a2) {
            return [SKAction sequence:@[a1, a2]];
        } else {
            return a1;
        }
    } else
        return a2;
}

- (MStop *)freeStop {
    NSMutableArray *availableAreas = [NSMutableArray array];
    NSMutableArray *area = [NSMutableArray array];
    for (MStop *stop in self.stops) {
        if ([stop isKindOfClass:[MWayPoint class]]) {
            if (area.count > 0) {
                [availableAreas addObject:area];
                area = [NSMutableArray array];
            }
        } else
            [area addObject:stop];
    }
    if (area.count > 0)
        [availableAreas addObject:area];
    if (availableAreas.count <= self.cars.count) return nil;
    for (MCar *car in self.cars) {
        for (NSArray *area in availableAreas) {
            if ([car.upcomingStop isKindOfClass:[MWayPoint class]]) {
                if ([area containsObject:car.previousStop]) {
                    [availableAreas removeObject:area];
                    break;
                }
            } else
                if ([area containsObject:car.upcomingStop]) {
                    [availableAreas removeObject:area];
                    break;
                }
        }
    }
    if (availableAreas.count > 0)
        return availableAreas[0][0];
    return nil;
}

- (void) runCar {
    if (self.stations.count > 1) {
        if (self.cars == nil)
            self.cars = [NSMutableArray array];
        while (self.cars.count < 1) {
#warning car count limit here ^^
#warning update car routes for standing ones
#warning remove cars too!
            MStop *stop = [self freeStop];
            if (stop == nil) return;
            MCar *car = [MCar spriteNodeWithImageNamed:@"metrotrain"];
            car.delegate = self;
            car.position = CGPointZero;//((MStation *)[self.stations firstObject]).position;
            car.carState = csRunningForward;
            car.upcomingStop = stop;
            SKAction *carRunAction = [self car:car moveToStop:car.upcomingStop Forward:car.carState == csRunningForward];
            [self.scene addChild:car];
            [car runAction:carRunAction];
            [self.cars addObject:car];
        }
    }
}

- (void) invalidate {
    changed = YES;
    [self finalizeEnds];
}

@end
