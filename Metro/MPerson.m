//
//  MPerson.m
//  Metro
//
//  Created by Danila Parhomenko on 4/2/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MPerson.h"

@implementation MPerson

@end

@implementation MPersonMove

+ (MPersonMove *)personLoadingAt:(MStation *)station toRide:(MLine *)line {
    MPersonMove *m = [[MPersonMove alloc] init];
    m.load = YES;
    m.station = station;
    m.line = line;
    return m;
}

+ (MPersonMove *)personUnloadingAt:(MStation *)station {
    MPersonMove *m = [[MPersonMove alloc] init];
    m.load = NO;
    m.station = station;
    return m;
}

@end