//
//  MStop.h
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class MLine;
@class MStop;
@class MCar;

@protocol DragDelegate

- (void)draggingStarted:(MStop *)sender Touch:(UITouch *)touch;
- (void)draggingMoved:(MStop *)sender Touch:(UITouch *)touch;
- (void)draggingEnded:(MStop *)sender Touch:(UITouch *)touch;
- (void)draggingCanceled:(MStop *)sender Touch:(UITouch *)touch;

- (void)personsDelivered:(NSSet *)persons;

@end

@interface MLineConnection : NSObject

@property (nonatomic, strong) MLine *line;
@property (nonatomic) int inDirx;
@property (nonatomic) int inDiry;
@property (nonatomic) int outDirx;
@property (nonatomic) int outDiry;
@property (nonatomic) CGPoint lastConnectionPoint;

@end

@interface MStop : SKSpriteNode

@property (nonatomic, weak) id<DragDelegate> delegate;

- (CGPoint) positionForLine:(MLine *)line andCar:(MCar *)car;

- (BOOL) tryConnectToLine:(MLine *)line inDirX:(int)inx inDirY:(int)iny outDirX:(int)outx outDirY:(int)outy;
- (void) disconnectFromLine:(MLine *)line;


@end
