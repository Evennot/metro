//
//  MCar.h
//  Metro
//
//  Created by Danila Parhomenko on 4/1/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MStation.h"

#define CarCapacity 10

typedef enum CarStateTag {
    csNone = 0,
    csRouteInvalidated = 1,
    csRunningForward = 2,
    csRunningBackward = 3,
    csDisappearing = 4,
#warning add loading/unloading
} CarState;

@protocol RouteCalculator

- (void)updateCarRoute:(MCar *)car;

@end
@interface MCar : SKSpriteNode

@property (nonatomic, strong) MStop *upcomingStop;
@property (nonatomic, strong) MStop *previousStop;
@property (nonatomic, strong) NSMutableArray *persons;
@property (atomic) CarState carState;
@property (nonatomic) CGFloat angle;

@property (nonatomic, weak) id<RouteCalculator> delegate;

- (void)updateCarRoute;

@end
