//
//  MStation.m
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MPerson.h"
#import "MCar.h"
#import "MStation.h"
#import "MLine.h"

#define ConnectionOffset 10

@implementation MStation {
    int personlocations[10];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.delegate) {
        [self.delegate draggingStarted:self Touch:[touches anyObject]];
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.delegate) {
        [self.delegate draggingEnded:self Touch:[touches anyObject]];
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.delegate) {
        [self.delegate draggingMoved:self Touch:[touches anyObject]];
    }
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.delegate) {
        [self.delegate draggingCanceled:self Touch:[touches anyObject]];
    }
}

- (void) assignPoint:(CGPoint) np toConnection:(MLineConnection *)c {
    if (!CGPointEqualToPoint(np, c.lastConnectionPoint)) {
        c.lastConnectionPoint = np;
        [c.line invalidate];
    }
}

- (void) rearrangeConnections {
    switch (self.lineConnections.count) {
        case 0:
            break;
        case 1:
            [self assignPoint:self.position toConnection:[self.lineConnections firstObject]];
            break;
        case 2:
            [self assignPoint:CGPointMake(self.position.x - ConnectionOffset, self.position.y - ConnectionOffset) toConnection:[self.lineConnections firstObject]];
            [self assignPoint:CGPointMake(self.position.x + ConnectionOffset, self.position.y + ConnectionOffset) toConnection:self.lineConnections[1]];
            break;
        case 3:
            [self assignPoint:CGPointMake(self.position.x - ConnectionOffset, self.position.y - ConnectionOffset) toConnection:self.lineConnections[0]];
            [self assignPoint:CGPointMake(self.position.x + ConnectionOffset, self.position.y - ConnectionOffset)toConnection:self.lineConnections[1]];
            [self assignPoint:CGPointMake(self.position.x, self.position.y + ConnectionOffset)toConnection:self.lineConnections[2]];
            break;
        default:
            [self assignPoint:CGPointMake(self.position.x - ConnectionOffset, self.position.y - ConnectionOffset) toConnection:self.lineConnections[0]];
            [self assignPoint:CGPointMake(self.position.x + ConnectionOffset, self.position.y - ConnectionOffset)toConnection:self.lineConnections[1]];
            [self assignPoint:CGPointMake(self.position.x - ConnectionOffset, self.position.y + ConnectionOffset)toConnection:self.lineConnections[2]];
            [self assignPoint:CGPointMake(self.position.x + ConnectionOffset, self.position.y + ConnectionOffset)toConnection:self.lineConnections[3]];
            for (int i = 4; i < self.lineConnections.count; i++)
            [self assignPoint:self.position toConnection:self.lineConnections[i]];
            break;
    }

}

- (BOOL)tryConnectToLine:(MLine *)line inDirX:(int)inx inDirY:(int)iny outDirX:(int)outx outDirY:(int)outy {
    MLineConnection *newC = nil;
    @synchronized (self) {
        if (self.lineConnections == nil)
            self.lineConnections = [NSMutableArray array];
        for (MLineConnection *c in self.lineConnections) {
            if ([c.line isEqual:line]) {
                c.inDirx = inx;
                c.inDiry = iny;
                c.outDirx = outx;
                c.outDiry = outy;
                return YES;
            }
        }
        newC = [[MLineConnection alloc] init];
        newC.line = line;
        [self.lineConnections addObject:newC];
        [self rearrangeConnections];
    }
    return YES;
#warning limit connection count per station here
}

- (CGPoint) positionForLine:(MLine *)line andCar:(MCar *)car {
    if (self.lineConnections)
        @synchronized (self) {
            for (MLineConnection *c in self.lineConnections) {
                if ([c.line isEqual:line]) {
                    return c.lastConnectionPoint;
                }
            }
        }
    return self.position;
}

- (void) disconnectFromLine:(MLine *)line {
    if (self.lineConnections)
        @synchronized (self) {
            for (MLineConnection *c in self.lineConnections) {
                if ([c.line isEqual:line]) {
                    [self.lineConnections removeObject:c];
                    [self rearrangeConnections];
                    return;
                }
            }
        }
}

- (void) removePerson:(MPerson *) person {
    [self.persons removeObject:person];
    NSNumber *x = person.userData[@"nodei"];
    NSNumber *y = person.userData[@"nodej"];
    personlocations[x.intValue] ^= y.intValue;
}

- (CGPoint) addPerson:(MPerson *) person {
    @synchronized (self) {
        if (self.persons == nil) {
            self.persons = [NSMutableArray array];
            memchr(personlocations, 0, sizeof(personlocations));
        }
        [self.persons addObject:person];
        int x = 0;
        int y = 0;
        for (; x < 10; x++) {
            y = 0;
            for (int j = 1; j <= 16; j<<=1) {
                if ((personlocations[x]&j) == 0) {
                    if (person.userData == nil) {
                        person.userData = [NSMutableDictionary dictionary];
                    }
                    person.userData[@"nodei"] = @(x);
                    person.userData[@"nodej"] = @(j);
                    personlocations[x] ^= j;
                    return CGPointMake(x * 10 + self.position.x + 20, y * 10 + self.position.y);
                }
                y++;
            }
        }
    }
#warning add "could not add reply here"
    return CGPointMake(self.position.x + 20, self.position.y);
}

- (SKAction *)unloadingActionForCar:(MCar *)car AtLine:(MLine *)line {
    int inCount = 0;
    NSMutableSet *personsDelivered = nil;
    @synchronized (self) {
        for (int i = car.persons.count; i--; ) {
            MPerson *p = car.persons[i];
            if ([line.delegate person:p needsToBeUnloadedAt:self fromCarRunningOn:line]) {
                if ([p.destination isEqual:self]) {
                    [p removeAllActions];
                    [p removeFromParent];
                } else {
                    p.lineChanges = p.lineChanges + 1;
                    p.alpha = 1.0;
                    p.position = [self addPerson:p];
                    inCount++;
                }
                [car.persons removeObject:p];
                if (personsDelivered == nil)
                    personsDelivered = [NSMutableSet set];
                [personsDelivered addObject:p];
            }
        }
    }
    if (self.delegate)
        [self.delegate personsDelivered:personsDelivered];

    if (inCount) {
        return [SKAction waitForDuration:0.1*inCount];
    }
    return nil;
}

- (SKAction *)loadingActionForCar:(MCar *)car AtLine:(MLine *)line {
    NSMutableArray *personsLeft = [NSMutableArray array];
    @synchronized (self) {
        if (car.persons == nil)
            car.persons = [NSMutableArray arrayWithCapacity:CarCapacity];
        for (int i = self.persons.count; i--; ) {
            if (car.persons.count >= CarCapacity)
                break;
            MPerson *p = self.persons[i];
            if ([line.delegate person: p needsToBeLoadedAt:self toRunOn:line onACarThatRunsForward:YES]) {
#warning add direction
                [personsLeft addObject:p];
                [car.persons addObject:p];
                [self removePerson:p];
                [p runAction:[SKAction fadeAlphaTo:0 duration:0.3]];
            }
        }
    }
    if (personsLeft.count > 0) {
        return [SKAction waitForDuration:0.1 * personsLeft.count];
    }
    return nil;
}



@end
