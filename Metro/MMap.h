//
//  MMap.h
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MStation.h"
#import "MWayPoint.h"
#import "MLine.h"

@protocol MapDelegate

- (void) personAdded:(MPerson *)person;
- (void) personsServed:(NSSet *)persons;
- (void) lineAdded;
- (void) lineRemoved;

@end

@interface MMap : SKScene <DragDelegate, MapProtocol>

@property (nonatomic, strong) NSMutableArray *stations;
@property (nonatomic, strong) NSMutableArray *waypoints;
#warning merge them
@property (nonatomic, strong) NSMutableArray *lines;
@property (nonatomic, strong) MLine *draftLine;
@property (nonatomic, strong) MLine *correctedLine;

@property (nonatomic, weak) id<MapDelegate> delegate;

- (void)reset;

@end
