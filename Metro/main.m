//
//  main.m
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAppDelegate class]));
    }
}
