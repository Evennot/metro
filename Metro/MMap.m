//
//  MMap.m
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MMap.h"
#import "MPerson.h"

#define LowColor 0.3
#define MidColor 0.6
#define HighColor 1.0

@interface MMap ()

@property BOOL contentCreated;

@end

@implementation MMap {
    CFTimeInterval lastResizeTime;
    CFTimeInterval stationAddingTime;
    CFTimeInterval personCreationTime;
    double continuousResizeFactor;
    int spawnCounter;
}

- (BOOL) couldFindSpawnPoint:(CGPoint *)spawnPoint {
    CGPoint point;
    CGRect inside = CGRectZero;
    inside.size = self.size;
    inside = CGRectInset(inside, 200, 200);
    if (self.stations.count == 0) {
        point = CGPointMake(inside.size.width * 0.5, inside.size.height * 0.5);
    } else {
        CGFloat ort = 15000*continuousResizeFactor*continuousResizeFactor;
        BOOL tooClose = NO;
        for (int i = 200; i--; ) {
            point = CGPointMake(inside.size.width * rand() * 1.0  / RAND_MAX, inside.size.height * 1.0  * rand() / RAND_MAX);
            point.x += inside.origin.x;
            point.y += inside.origin.y;
            tooClose = NO;
            for (MStation *s1 in self.stations) {
                CGFloat dx = s1.position.x - point.x;
                CGFloat dy = s1.position.y - point.y;
                CGFloat distance = dx*dx + dy*dy;
                if (distance < ort) {
                    tooClose = YES;
                    break;
                }
            }
            if (!tooClose) {
                for (MWayPoint *s1 in self.waypoints) {
                    CGFloat dx = s1.position.x - point.x;
                    CGFloat dy = s1.position.y - point.y;
                    CGFloat distance = dx*dx + dy*dy;
                    if (distance < ort) {
                        tooClose = YES;
                        break;
                    }
                }
            }
            if (!tooClose)
                break;
        }
        if (tooClose)
            return NO;
    }
    *spawnPoint = point;
    return YES;
}

- (void) addRandomStation {
    CGPoint point;
    if (![self couldFindSpawnPoint:&point]) return;
    spawnCounter++;
    MStop *stop = nil;
    if (false && (spawnCounter % 4 == 3)) {
        MWayPoint *waypoint = [MWayPoint spriteNodeWithImageNamed:@"waypoint"];
        stop = waypoint;
        [self.waypoints addObject:waypoint];
    } else {
        MStation *station = [MStation spriteNodeWithImageNamed:@"pawn"];
        station.userInteractionEnabled = YES;
        stop = station;
        [self.stations addObject:station];
    }
    [stop setZPosition:1];
    stop.position = point;
    stop.delegate = self;

    [self addChild:stop];
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [SKColor whiteColor];
        //self.anchorPoint = CGPointZero;//CGPointMake(size.width * 0.5, size.height * 0.5);
        self.scaleMode = SKSceneScaleModeAspectFill;
    }
    return self;
}

- (void)reset {
    self.stations = [NSMutableArray array];
    self.waypoints = [NSMutableArray array];
    self.lines = [NSMutableArray array];
    continuousResizeFactor = 1.0;
    [self addRandomStation];
    [self addRandomStation];
    [self addRandomStation];
    [self addRandomStation];
    lastResizeTime = 0;
    stationAddingTime = 0;
    personCreationTime = 0;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        for (MStation *s in self.stations) {
            if ([s containsPoint:location])
                return;
        }
    }
//    for (UITouch *touch in touches) {
//        CGPoint location = [touch locationInNode:self];
//        for (MWayPoint *s in self.waypoints) {
//            if ([s containsPoint:location])
//                return;
//        }
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UITouch *touch in touches) {
            CGPoint location = [touch locationInNode:self];
            for (MLine *l in self.lines) {
                if ([l tryDragging:location]) {
                    self.correctedLine = l;
                    return;
                }
            }
        }
    });
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.correctedLine) {
        CGPoint point = [[touches anyObject] locationInNode:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            for (MStation *s in self.stations) {
                if ([s containsPoint:point]) {
                    [self.correctedLine draggedToStop:s];
                    [self.correctedLine moveTo:point OutOfStation:NO];
                    return;
                }
            }
            for (MWayPoint *s in self.waypoints) {
                if ([s containsPoint:point]) {
                    [self.correctedLine draggedToStop:s];
                    [self.correctedLine moveTo:point OutOfStation:NO];
                    return;
                }
            }
            [self.correctedLine moveTo:point OutOfStation:YES];
        });
    }
}

- (void) endCorrection {
    if (self.correctedLine)
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.correctedLine.stations.count <= 1) {
                [self.correctedLine removeFromParent];
                [self.lines removeObject:self.correctedLine];
                if (self.delegate)
                    [self.delegate lineRemoved];
            } else {
                [self.correctedLine finalizeEnds];
            }
            self.correctedLine = nil;
        });
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endCorrection];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endCorrection];
}

-(void)update:(CFTimeInterval)currentTime {
    if (self.draftLine) {
        [self.draftLine commitAnimations];
    }
    for (MLine *l in self.lines) {
        [l commitAnimations];
    }
    if ((currentTime - lastResizeTime > 1.0) && (continuousResizeFactor < 1.96)) {
        lastResizeTime = currentTime;
        continuousResizeFactor *= 1.001;
        self.size = CGSizeMake(self.size.width * 1.001, self.size.height * 1.001);
//        for (SKNode *node in self.children) {
//            node.position = CGPointMake(node.position.x * 1.001*0.5, node.position.y * 1.001*0.5);
//        }
    }
    if (currentTime - stationAddingTime > 30.0) {
        [self addRandomStation];
        stationAddingTime = currentTime;
    }
    if (currentTime - personCreationTime > 1.0*2.0/self.stations.count) {
        personCreationTime = currentTime;
        int sIndex = self.stations.count * 1.0 * rand() / RAND_MAX;
        int sIndex2;
        while (true) {
            sIndex2 = self.stations.count * 1.0 * rand() / RAND_MAX;
            if (sIndex2 != sIndex)
                break;
        }
        MPerson *person = [MPerson spriteNodeWithImageNamed:@"dot"];
        person.source = self.stations[sIndex];
        person.position = [person.source addPerson:person];
        person.destination = self.stations[sIndex2];
        person.startTime = CFAbsoluteTimeGetCurrent();
        [person setZPosition:2.0];
        if (person.parent == nil) {
            [self addChild:person];
            if (self.delegate)
                [self.delegate personAdded:person];
        } else
            NSLog(@"Person could not be created");
    }
}

- (void) draggingStarted:(MStation *)sender Touch:(UITouch *)touch {
#warning dispose old line
    dispatch_async(dispatch_get_main_queue(), ^{
        self.draftLine = [[MLine alloc] initWithColor:[UIColor grayColor] startStation:sender Delegate:self];
        [self addChild:self.draftLine];
        [self.draftLine setZPosition:-5];
    });
}

- (void) draggingMoved:(MStation *)sender Touch:(UITouch *)touch {
    CGPoint point = [touch locationInNode:self];
    dispatch_async(dispatch_get_main_queue(), ^{
        for (MStation *s in self.stations) {
            if ([s containsPoint:point]) {
                [self.draftLine draggedToStop:s];
                [self.draftLine moveTo:point OutOfStation:NO];
                return;
            }
        }
        for (MStation *s in self.waypoints) {
            if ([s containsPoint:point]) {
                [self.draftLine draggedToStop:s];
                [self.draftLine moveTo:point OutOfStation:NO];
                return;
            }
        }
        [self.draftLine moveTo:point OutOfStation:YES];

    });
}

- (void) draggingCanceled:(MStation *)sender Touch:(UITouch *)touch {
    [self.draftLine removeFromParent];
    self.draftLine = nil;
}

int indexByFloat(CGFloat c) {
    if (c < MidColor - 0.1) {
        if (c < LowColor - 0.1)
            return 0;
        return 1;
    }
    if (c < HighColor - 0.1)
        return 2;
    return 3;
}

CGFloat floatByIndex(int index) {
    if (index < 2) {
        if (index == 1)
            return LowColor;
        return 0;
    } else {
        if (index == 2)
            return MidColor;
        return HighColor;
    }
}
- (UIColor *)generateNewColor {
    NSMutableArray *indexes = [NSMutableArray array];
    for (MLine *l in self.lines) {
        UIColor *c = l.strokeColor;
        CGFloat r, g, b, a;
        [c getRed:&r green:&g blue:&b alpha:&a];
        NSUInteger index = (indexByFloat(r) << 4) || (indexByFloat(g) << 2) || (indexByFloat(b));
        [indexes addObject:@(index)];
    }
    while (true) {
        NSUInteger index = 0x3F*1.0*rand()/RAND_MAX;
        for (NSNumber *n in indexes)
            if (n.integerValue == index) {
                index = -1;
                break;
            }
        if (index > 0)
            return [UIColor colorWithRed:floatByIndex(index&3) green:floatByIndex((index>>2)&3) blue:floatByIndex((index>>4)&3)
                                   alpha:1.0];
#warning bug here
    }
}

- (void) draggingEnded:(MStation *)sender Touch:(UITouch *)touch {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.draftLine.stations.count > 1) {
            [self.lines addObject:self.draftLine];
            if (self.delegate)
                [self.delegate lineAdded];
            [self.draftLine setStrokeColor:[self generateNewColor]];
//            switch (self.lines.count) {
//                case 1:
//                    [self.draftLine setStrokeColor:[UIColor redColor]];
//                    break;
//                case 2:
//                    [self.draftLine setStrokeColor:[UIColor greenColor]];
//                    break;
//                case 3:
//                    [self.draftLine setStrokeColor:[UIColor blueColor]];
//                    break;
//                case 4:
//                    [self.draftLine setStrokeColor:[UIColor cyanColor]];
//                    break;
//                case 5:
//                    [self.draftLine setStrokeColor:[UIColor brownColor]];
//                    break;
//                case 6:
//                    [self.draftLine setStrokeColor:[UIColor orangeColor]];
//                    break;
//                case 7:
//                    [self.draftLine setStrokeColor:[UIColor purpleColor]];
//                    break;
//                default:
//                    [self.draftLine setStrokeColor:[UIColor darkGrayColor]];
//                    break;
//            }
            [self.draftLine finalizeEnds];
        } else {
            [self.draftLine removeFromParent];
        }
    });
//    [self.draftLine removeFromParent];
#warning not this, actually
}

- (BOOL) point:(CGPoint)p belongsToLineOtherThan:(MLine *)line {
    for (MLine *l in self.lines) {
        if (![l isEqual:line] && [l pointBelongsToLine:p])
            return YES;
    }
    return NO;
}

- (NSArray *) desiredLinesForAPerson:(MPerson *)p {
    NSMutableArray *res = [NSMutableArray array];
    for (MLine *l in self.lines) {
        for (MStation *s in l.stations) {
            if ([s isEqual:p.destination]) {
                if (![res containsObject:l])
                    [res addObject:l];
            }
        }
    }
    return res;
}

- (NSArray *) allLinesConnectedTo:(NSArray *)firstRankLines {
    NSMutableArray *nextRankLines = [NSMutableArray arrayWithArray:firstRankLines];
    for (MLine *l in firstRankLines) {
        for (MStation *s in l.stations) {
            if (s.lineConnections.count > 1)
                for (MLineConnection *c in s.lineConnections) {
                    if (![nextRankLines containsObject:c.line]) {
                        [nextRankLines addObject:c.line];
                    }
                }
        }
    }
    return nextRankLines;
}

- (BOOL) person:(MPerson *) p needsToBeLoadedAt:(MStation *) station toRunOn:(MLine *)line onACarThatRunsForward:(BOOL)forward {
    NSArray *desiredLines = [self desiredLinesForAPerson:p];
#warning concern forward/backward
    if (desiredLines && (desiredLines.count > 0)) {
        for (MLine *l in desiredLines)
            if ([l isEqual:line]) { // we've got to target line
                return YES;
            }
        for (MLineConnection *c in station.lineConnections) {
#warning temporary check for connection availability
            if (![c.line.stations containsObject:station] || (c.line.scene == nil)) {
                NSLog(@"WTF?");
                [station disconnectFromLine:c.line];
                return [self person:p needsToBeLoadedAt:station toRunOn:line onACarThatRunsForward:forward];
            }
            if ([desiredLines containsObject:c.line]) { // right station, wrong line
                return NO;
            }
        }
        NSUInteger lastCount = desiredLines.count;
        NSArray *firstRankLines = desiredLines;
        while (true) {
            NSArray *nextRankLines = [self allLinesConnectedTo:firstRankLines]; // all lines crossed with desired ones
            if ([nextRankLines containsObject:line]) {
                return YES;
            }
            for (MLineConnection *c in station.lineConnections)
                if ([nextRankLines containsObject:c.line]) { // right station, wrong line
                    return NO;
                }
            if (nextRankLines.count == lastCount)
                return NO;
            lastCount = nextRankLines.count;
            firstRankLines = nextRankLines;
        }
    } else // station is not connectied
        return NO;
}

- (BOOL) person:(MPerson *)p needsToBeUnloadedAt:(MStation *)station fromCarRunningOn:(MLine *)line {
    if ([station isEqual:p.destination])
        return YES;
    NSArray *desiredLines = [self desiredLinesForAPerson:p];
    if ([desiredLines containsObject:line])
        return NO;
    for (MLineConnection *c in station.lineConnections) {
        if ([desiredLines containsObject:c.line]) {
            return YES;
        }
    }
    NSUInteger lastCount = desiredLines.count;
    while (true) {
        NSArray *nextRankLines = [self allLinesConnectedTo:desiredLines]; // all lines crossed with desired ones
        if ([nextRankLines containsObject:line])
            return NO;
        for (MLineConnection *c in station.lineConnections) {
            if ([nextRankLines containsObject:c.line]) {
                return YES;
            }
        }
        if (nextRankLines.count == lastCount)
            return NO;
        lastCount = nextRankLines.count;
        desiredLines = nextRankLines;
    }
}

- (void) personsDelivered:(NSSet *)persons {
    if (self.delegate)
        [self.delegate personsServed:persons];
}

@end
