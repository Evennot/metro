//
//  MPerson.h
//  Metro
//
//  Created by Danila Parhomenko on 4/2/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class MStation;
@class MLine;

@interface MPersonMove : NSObject

@property (nonatomic) BOOL load;
@property (nonatomic, strong) MLine *line; // does not count if load == false
@property (nonatomic, strong) MStation *station;

+ (MPersonMove *)personLoadingAt:(MStation *)station toRide:(MLine *)line;
+ (MPersonMove *)personUnloadingAt:(MStation *)station;

@end

@interface MPerson : SKSpriteNode

@property (nonatomic, strong) NSMutableArray *moves;
@property (nonatomic, weak) MStation *source;
@property (nonatomic, weak) MStation *destination;
@property (nonatomic) NSUInteger lineChanges;
@property (nonatomic) CFAbsoluteTime startTime;

@end
