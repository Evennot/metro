//
//  MWayPoint.h
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MStation.h"
#import "MStop.h"

@interface MWayPoint : MStop

@property (nonatomic, strong) MLineConnection *lineConnection;
@property (atomic, strong) NSMutableArray *carsWaiting;

@end
