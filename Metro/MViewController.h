//
//  MViewController.h
//  Metro
//

//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "MMap.h"

@interface MViewController : UIViewController <MapDelegate>

@property (weak, nonatomic) IBOutlet SKView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *servedLabel;
@property (weak, nonatomic) IBOutlet UILabel *waitingLabel;

@end
