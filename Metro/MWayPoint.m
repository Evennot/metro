//
//  MWayPoint.m
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MWayPoint.h"
#import "MLine.h"

@interface MWayPoint ()


@end

@implementation MWayPoint

- (BOOL)tryConnectToLine:(MLine *)line inDirX:(int)inx inDirY:(int)iny outDirX:(int)outx outDirY:(int)outy {
    if (self.lineConnection != nil)
        return [self.lineConnection.line isEqual:line];
    @synchronized (self) {
        self.lineConnection = [[MLineConnection alloc] init];
        self.lineConnection.line = line;
    }
    return YES;
}

- (CGPoint) positionForLine:(MLine *)line andCar:(MCar *)car {
    return self.position;
}

- (void) disconnectFromLine:(MLine *)line {
    if (self.lineConnection)
        @synchronized (self) {
            if ([self.lineConnection.line isEqual:line]) {
                self.lineConnection = nil;
            }
        }
    self.carsWaiting = nil;
}


@end
