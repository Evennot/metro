//
//  MStation.h
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MStop.h"

@class MPerson;
@class MCar;
@class MStation;
@class MLine;

typedef enum StationDockTag {
    dNW = 0x00, // north west
    dN  = 0x01,
    dNE = 0x02,
    dE  = 0x04,
    dSE = 0x08,
    dS  = 0x10,
    dSW = 0x20,
    dW  = 0x40,
} StationDock;

@interface MStation : MStop

@property (nonatomic, weak) id<DragDelegate> delegate;
@property (nonatomic) NSInteger docs;
@property (nonatomic, strong) NSMutableArray *lineConnections;
@property (nonatomic, strong) NSMutableArray *persons;

//- (CGPoint) positionForLine:(MLine *)line;
//
//- (CGPoint) connectToLine:(MLine *)line inDirX:(int)inx inDirY:(int)iny outDirX:(int)outx outDirY:(int)outy;
//- (void) disconnectFromLine:(MLine *)line;
- (CGPoint) addPerson:(MPerson *) person;
- (SKAction *)loadingActionForCar:(MCar *)car AtLine:(MLine *)line;
- (SKAction *)unloadingActionForCar:(MCar *)car AtLine:(MLine *)line;

@end
