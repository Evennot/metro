//
//  MStop.m
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MStop.h"

@implementation MStop

- (CGPoint) positionForLine:(MLine *)line andCar:(MCar *)car {
    return self.position;
}

- (BOOL) tryConnectToLine:(MLine *)line inDirX:(int)inx inDirY:(int)iny outDirX:(int)outx outDirY:(int)outy {
    return NO;
}

- (void) disconnectFromLine:(MLine *)line {
}

@end

@implementation MLineConnection

@end