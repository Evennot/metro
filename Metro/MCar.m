//
//  MCar.m
//  Metro
//
//  Created by Danila Parhomenko on 4/1/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MCar.h"

@implementation MCar

- (void)updateCarRoute {
    if (self.delegate)
        [self.delegate updateCarRoute:self];
}

@end
