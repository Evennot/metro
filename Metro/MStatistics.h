//
//  MStatistics.h
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPerson.h"

@interface MStatistics : NSObject

@property (nonatomic, readonly, getter = getPersonsWaiting) NSInteger personsWaiting;
@property (nonatomic, readonly, getter = getPersonsServed) NSInteger personsServed;
@property (nonatomic, readonly, getter = getMeanWaitingTime) NSTimeInterval meanWaitingTime;
@property (nonatomic, readonly, getter = getMeanLineChangeRate) double meanLineChangeRate;

- (void)addPerson:(MPerson *)person;
- (void)removePerson:(MPerson *)person;

@end
