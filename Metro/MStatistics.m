//
//  MStatistics.m
//  Metro
//
//  Created by Danila Parhomenko on 4/3/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MStatistics.h"
#import <Accelerate/Accelerate.h>
#define MeansInterval 10.0
#define MeanStructStride 4

typedef struct MeansTag {
    float meanWaitingTime;
    float meanLineChangeRate;
    float served;
    float tmp;
} MeansStruct;

@interface MStatistics ()

@property (nonatomic, strong) NSMutableArray *trackedPersons;

@end

@implementation MStatistics {
    MeansStruct *means; // by 10 second intervals
    NSInteger meansCapacity, meansCount;
    CFAbsoluteTime meansStart;
    NSInteger served;
    double totalWaitingTime;
    NSInteger totalLineChanges;
}

- (id) init {
    if (self = [super init]) {
        meansCapacity = 16;
        means = malloc(meansCapacity * sizeof(MeansStruct));
        meansCount = 0;
        meansStart = -1;
    }
    return self;
}

- (void) dealloc {
    free(means);
}

- (void) growMeans {
    if (meansCount == meansCapacity) {
        meansCapacity = (meansCapacity + 1)*2;
        MeansStruct *newMeans = malloc(meansCapacity);
        memcpy(newMeans, means, meansCount * sizeof(MeansStruct));
        free(means);
        means = newMeans;
    }
    means[meansCount].meanWaitingTime = 0;
    means[meansCount].meanLineChangeRate = 0;
    means[meansCount].served = 0;
    meansCount++;
    
}

- (void)addPerson:(MPerson *)person {
    @synchronized (self) {
        if (self.trackedPersons == nil) {
            self.trackedPersons = [NSMutableArray array];
            meansStart = CFAbsoluteTimeGetCurrent();
        }
        [self.trackedPersons addObject:person];
    }
}

- (void)removePerson:(MPerson *)person {
    @synchronized (self) {
        CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
//        double diff = t - meansStart;
//        int intDiff = diff/MeansInterval;
//        while (intDiff >= meansCount) {
//            [self growMeans];
//        }
//        means[intDiff].meanWaitingTime += t - person.startTime;
//        means[intDiff].served += 1.0;
//        means[intDiff].meanLineChangeRate += person.lineChanges;
        served++;
        totalWaitingTime += t - person.startTime;
        totalLineChanges += person.lineChanges;
        [self.trackedPersons removeObject:person];
    }
}

- (NSInteger) getPersonsWaiting {
    return self.trackedPersons.count;
}

- (NSInteger) getPersonsServed {
    @synchronized (self) {
//        float summ = 0;
//        vDSP_sve(&(means[0].served), MeanStructStride, &summ, meansCount);
//        return summ;
        return served;
    }
}

- (NSTimeInterval) getMeanWaitingTime {
    @synchronized (self) {
//        vDSP_vdiv(&(means[0].meanWaitingTime), MeanStructStride, &(means[0].served), MeanStructStride, &(means[0].tmp), MeanStructStride, meansCount);
//        float v = 0;
//        vDSP_sve(&(means[0].tmp), MeanStructStride, &v, meansCount);
//        return v;
        return totalWaitingTime / served;
    }
}

- (double) getMeanLineChangeRate {
    @synchronized (self) {
//        vDSP_vdiv(&(means[0].meanLineChangeRate), MeanStructStride, &(means[0].served), MeanStructStride, &(means[0].tmp), MeanStructStride, meansCount);
//        float v = 0;
//        vDSP_sve(&(means[0].tmp), MeanStructStride, &v, meansCount);
//        return v;
        return totalLineChanges * 1.0 / served;
    }
}


@end
