//
//  MViewController.m
//  Metro
//
//  Created by Danila Parhomenko on 3/31/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import "MViewController.h"
#import "MMyScene.h"
#import "MStatistics.h"

@interface MViewController ()

@property (nonatomic, strong) MMap *mapScene;
@property (nonatomic, strong) MStatistics *statistics;
@property (atomic) double money;
@end

@implementation MViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = self.mainView;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    self.mapScene = [MMap sceneWithSize:skView.bounds.size];
    self.mapScene.scaleMode = SKSceneScaleModeAspectFill;
    self.mapScene.delegate = self;
    // Present the scene.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    SKView * skView = self.mainView;
    [skView presentScene:self.mapScene];
    [self.mapScene reset];
    self.statistics = [[MStatistics alloc] init];
    self.money = 5000;
}

- (NSString *)abbreviatedInt:(NSInteger) v {
    if (v >= 1000) {
        if (v >= 100000)
            return [NSString stringWithFormat:@"%.0fK", v/1000.0];
        if (v >= 10000)
            return [NSString stringWithFormat:@"%.1fK", v/1000.0];
        return [NSString stringWithFormat:@"%.2fK", v/1000.0];
    }
    return [NSString stringWithFormat:@"%d", v];
}

- (void) updateStats {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.moneyLabel setText:[NSString stringWithFormat:@"%.2f", self.money]];
        [self.servedLabel setText:[self abbreviatedInt:self.statistics.personsServed]];
        [self.waitingLabel setText:[self abbreviatedInt:self.statistics.personsWaiting]];
    });
}

- (void) personAdded:(MPerson *)person {
    [self.statistics addPerson:person];
    [self updateStats];
}

- (void) personsServed:(NSSet *)persons {
    for (MPerson *person in persons)
        [self.statistics removePerson:person];
    self.money = self.money + 0.25*persons.count;
    [self updateStats];
}

- (void) lineAdded {
    self.money = self.money - 1000;
    [self updateStats];
}

- (void) lineRemoved {
    self.money = self.money + 990;
    [self updateStats];
}

@end
