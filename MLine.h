//
//  MLine.h
//  Metro
//
//  Created by Danila Parhomenko on 4/1/14.
//  Copyright (c) 2014 Danila Parhomenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "MStation.h"
#import "MCar.h"

@class MLine;

@protocol MapProtocol

- (BOOL) point:(CGPoint) p belongsToLineOtherThan:(MLine *)line;
- (BOOL) person:(MPerson *) p needsToBeLoadedAt:(MStation *) station toRunOn:(MLine *)line onACarThatRunsForward:(BOOL)forward;
- (BOOL) person:(MPerson *) p needsToBeUnloadedAt:(MStation *) station fromCarRunningOn:(MLine *)line;

@end

@interface MLine : SKShapeNode <RouteCalculator>

@property (nonatomic, strong) NSMutableArray *stations;
@property (nonatomic, strong) NSMutableArray *stops;
@property (nonatomic, readonly) BOOL cyclic;
@property (nonatomic, weak) id<MapProtocol> delegate;
//@property (nonatomic, strong) SKShapeNode
//SKShapeNode *yourline = [SKShapeNode node];
//CGMutablePathRef pathToDraw = CGPathCreateMutable();
//CGPathMoveToPoint(pathToDraw, NULL, 100.0, 100.0);
//CGPathAddLineToPoint(pathToDraw, NULL, 50.0, 50.0);
//yourline.path = pathToDraw;
//[yourline setStrokeColor:[UIColor redColor]];
//[self addChild:yourline];

- (id) initWithColor:(UIColor *)color startStation:(MStation *)station Delegate:(id<MapProtocol>) delegate;
- (void) draggedToStop:(MStop *)stop;
- (void) moveTo:(CGPoint) p OutOfStation:(BOOL)outOfStation;
- (void) finalizeEnds;
- (void) commitAnimations;
- (BOOL) tryDragging:(CGPoint) p;
- (BOOL) pointBelongsToLine:(CGPoint) p;
- (void) invalidate;

@end
